# Versions History

## v1.0.3

- added ```__str__ ``` function to easily include just the time stamp inside other strings

## v1.0.2

- updated the README
- added multiple streams functionality

## v1.0.1

- minor fixes

## v1.0.0

- first reelease
